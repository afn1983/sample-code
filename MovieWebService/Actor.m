//
//  Actor.m
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "Actor.h"

// Contants
static NSString *const MWScreenName			=	@"screenName";
static NSString *const MWDateOfBirth		=	@"dateOfBirth";
static NSString *const MWNominated			=	@"nominated";
static NSString *const MWBiography			=	@"biography";
static NSString *const MWName				=	@"name";

@implementation Actor

- (instancetype)initWithData:(NSDictionary *)data {
	
    self = [super init];
    if (self) {
        self.screenName		=	data[MWScreenName];
		self.dateOfBirth	=	[NSDate dateWithTimeIntervalSince1970:[data[MWDateOfBirth] doubleValue]];
		self.name			=	data[MWName];
		self.nominated		=	[data[MWNominated] boolValue];
		self.biography		=	data[MWBiography];
    }
	
    return self;
}

@end
