//
//  GenericRole.m
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "GenericRole.h"

// Contants
static NSString *const MWScreenName			=	@"screenName";
static NSString *const MWDateOfBirth		=	@"dateOfBirth";
static NSString *const MWNominated			=	@"nominated";
static NSString *const MWBiography			=	@"biography";
static NSString *const MWName				=	@"name";

@implementation GenericRole

- (instancetype)initWithData:(NSDictionary *)data {
	self = [super init];
	if (self) {
        self.name			= data[MWName];
        self.biography		= data[MWBiography];
        self.dateOfBirth	= [NSDate dateWithTimeIntervalSince1970:[data[MWDateOfBirth] doubleValue]];
        self.nominated		= [data[MWNominated] boolValue];
    }
    return self;
}

@end
