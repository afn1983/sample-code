//
//  ViewController.h
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "ListFilmViewModel.h"

@interface ListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
