//
//  GenericRole.h
//  MovieWebService
//
// on 3/10/14.
//  
//

@class Film;

@interface GenericRole : NSObject

@property (nonatomic, strong)	NSString	*name;
@property (nonatomic, strong)	NSString	*biography;
@property (nonatomic, strong)	NSDate		*dateOfBirth;
@property (nonatomic, assign)	BOOL		nominated;

@property (nonatomic, weak)		Film		*film;

- (instancetype)initWithData:(NSDictionary *)data;

@end
