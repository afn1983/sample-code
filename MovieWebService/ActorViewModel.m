//
//  ActorViewModel.m
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "ActorViewModel.h"

@implementation ActorViewModel

- (instancetype)initWithActor:(Actor *)actor {
	self = [super init];
	if (!self) return nil;
	
	_actor = actor;
	
	// Name
	_actorNameText = [actor.name stringIsNilOrEmpty] ? @"No name" : actor.name;
	
	// Screen Name
	
	// if the actor doesn't have a screen name , it uses the actor name
	_screenNameText = [actor.screenName stringIsNilOrEmpty] ? _actorNameText : actor.screenName;
	
	return self;
}

@end
