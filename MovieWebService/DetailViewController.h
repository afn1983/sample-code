//
//  DetailViewController.h
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "TappableLabel.h"
#import "FilmViewModel.h"

@interface DetailViewController : UIViewController <TappableLabelDelegate>

@property (weak, nonatomic) IBOutlet UILabel *directorTitle;
@property (weak, nonatomic) IBOutlet UILabel *directorName;

@property (weak, nonatomic) IBOutlet UILabel *actorNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *actorName;

@property (weak, nonatomic) IBOutlet UILabel *actorScreenNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *actorScreenName;

@property (weak, nonatomic) IBOutlet TappableLabel *tapToShowMore;

- (instancetype)initWithFilmViewModel:(FilmViewModel *)filmViewModel;

@end
