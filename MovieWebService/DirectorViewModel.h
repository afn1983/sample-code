//
//  DirectorViewModel.h
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "Director.h"
#import "Actor.h"

@interface DirectorViewModel : NSObject

@property (nonatomic, readonly) Director	*director;
@property (nonatomic, readonly) NSString	*nameText;
@property (nonatomic, readonly)	Film		*film;

- (instancetype)initWithDirector:(Director *)director;

- (Actor *)firstActor;

@end
