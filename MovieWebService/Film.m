//
//  Film.m
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "Film.h"
#import "Actor.h"
#import "Director.h"

// Contants
static NSString *const MWFilmRating			=	@"filmRating";
static NSString *const MWLanguages			=	@"languages";
static NSString *const MWNominated			=	@"nominated";
static NSString *const MWReleaseDate		=	@"releaseDate";
static NSString *const MWName				=	@"name";
static NSString *const MWRating				=	@"rating";
static NSString *const MWDirector			=	@"director";
static NSString *const MWCast				=	@"cast";

@implementation Film

- (instancetype)initWithData:(NSDictionary *)data {
    self = [super init];
    if (self) {
		
		self.filmRating		=	[data[MWFilmRating] integerValue];
        self.languages		=	data[MWLanguages];
        self.nominated		=	[data[MWNominated] boolValue];
        self.releaseDate	=	[NSDate dateWithTimeIntervalSince1970:[data [MWReleaseDate] doubleValue]];
        self.name			=	data[MWName];
        self.rating			=	[data[MWRating] floatValue];
		
        self.director		=	[[Director alloc] initWithData:data[MWDirector]];
		self.director.film	=	self;
		
		NSArray *castData = data[MWCast];
        NSMutableArray *casts = [[NSMutableArray alloc] initWithCapacity:castData.count];
		Actor *actor;
		
        for (NSDictionary *cast in castData) {
            actor = [[Actor alloc] initWithData:cast];
            actor.film = self;
            [casts addObject:actor];
        }
		
        self.casts = casts;
    }
	
    return self;
}

@end
