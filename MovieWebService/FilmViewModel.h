//
//  FilmViewModel.h
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "Film.h"

// Constants
static NSString *const MWFilmRating_G_Text			=	@"G";
static NSString *const MWFilmRating_PG_Text			=	@"PG";
static NSString *const MWFilmRating_PG13_Text		=	@"PG13";
static NSString *const MWFilmRating_R_Text			=	@"R";
static NSString *const MWFilmRating_NC17_Text		=	@"NC17";
static NSString *const MWFilmRating_Unknown_Text	=	@"Unknown";

@interface FilmViewModel : NSObject

@property (nonatomic, readonly) Film *film;

@property (nonatomic, readonly) NSString *nameText;
@property (nonatomic, readonly) NSString *releaseDateText;

@property (nonatomic, readonly) NSString *filmRatingText;
@property (nonatomic, readonly) NSString *ratingText;

- (instancetype)initWithFilm:(Film *)film;

- (NSString *)directorNameText;
- (NSString *)actorNameText;
- (NSString *)actorScreenNameText;

- (void)setDirectorNameText:(NSString *)directorNameText;
- (void)setActorNameText:(NSString *)actorNameText;
- (void)setActorScreenNameText:(NSString *)actorScreenNameText;

@end
