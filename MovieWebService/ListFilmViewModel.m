//
//  ListFilmViewModel.m
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "ListFilmViewModel.h"
#import "FilmViewModel.h"

@implementation ListFilmViewModel

+ (id)shared {
	static ListFilmViewModel *sharedMyManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedMyManager = [[self alloc] init];
	});
	return sharedMyManager;
}

- (void)loadSampleData:(void (^)(BOOL finished))completionHandler {
	
	NSMutableArray<FilmViewModel *> *sampleFilms = [NSMutableArray array];
	
	NSDictionary *filmADict = @{
							@"filmRating" : @3,
							@"languages": @[
									@"English",
									@"Thai"
									],
							@"nominated": @1,
							@"releaseDate": @1350000000,
							@"cast": @[
									@{
										@"dateOfBirth": @-436147200,
										@"nominated": @1,
										@"name": @"Bryan Cranston",
										@"screenName": @"Jack Donnell",
										@"biography": @"Bryan Lee Cranston is an American actor, voice actor, writer and director."
										}
									],
							@"name": @"Argo",
							@"rating": @7.8,
							@"director": @{
									@"dateOfBirth": @82684800,
									@"nominated": @1,
									@"name": @"Ben Affleck",
									@"biography": @"Benjamin Geza Affleck was born on August 15, 1972 in Berkeley, California, USA but raised in Cambridge, Massachusetts, USA."
									}
							};
	
	
	Film *filmA = [[Film alloc] initWithData:filmADict];
	
	if (filmA)
		[sampleFilms addObject:[[FilmViewModel alloc] initWithFilm:filmA]];
	
	_films = [NSMutableArray array];
	
	// KVO
	NSMutableArray * eventsArrayProxy = [self mutableArrayValueForKey:NSStringFromSelector(@selector(films))]; // this will trigger the KVO
	[eventsArrayProxy addObjectsFromArray:sampleFilms];
		
	if (completionHandler) {
		completionHandler(YES);
	}
}

@end
