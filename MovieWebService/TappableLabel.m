//
//  TappableLabel.m
//  MovieWebService
//
//
//

#import "TappableLabel.h"

@implementation TappableLabel

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		[self commonInit];
    }
    return self;
}

- (void)commonInit {
	self.userInteractionEnabled = YES;
	
	UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
	[self addGestureRecognizer:gesture];
}

- (void)onTapGesture:(UITapGestureRecognizer *)gesture {
	if ([self.delegate respondsToSelector:@selector(onDidReceiveTouch:)]) {
		[self.delegate onDidReceiveTouch:self];
	}
}

@end
