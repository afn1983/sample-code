//
//  MovieTableViewCell.m
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "MovieTableViewCell.h"

@implementation MovieTableViewCell

- (void)setupCell:(FilmViewModel *)filmViewModel {
		
    self.name.text			= filmViewModel.nameText;
	self.date.text			= filmViewModel.releaseDateText;
	self.filmRating.text	= filmViewModel.filmRatingText;
	self.rating.text		= filmViewModel.ratingText;
}

@end
