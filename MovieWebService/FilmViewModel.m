//
//  FilmViewModel.m
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "FilmViewModel.h"
#import "DirectorViewModel.h"
#import "ActorViewModel.h"

@implementation FilmViewModel

- (instancetype)initWithFilm:(Film *)film {
	self = [super init];
	if (!self) return nil;

	_film = film;
	
	// Name
	_nameText = film.name == nil || [film.name stringIsNilOrEmpty] ? @"No name" : film.name;
	
	// Release date
	NSCalendar *cal = [NSCalendar currentCalendar];
	
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	[df setCalendar:cal];
	[df setDateStyle:NSDateFormatterMediumStyle];
	
	NSString *dateText = [df stringFromDate:film.releaseDate];
	
	_releaseDateText = [dateText stringIsNilOrEmpty] ? @"No release date" : dateText;
	
	// Film rating
	NSString *filmRatingText;
	
	switch (film.filmRating) {
		case MWFilmRating_G:
			filmRatingText = MWFilmRating_G_Text;
			break;
		case MWFilmRating_PG:
			filmRatingText = MWFilmRating_PG_Text;
			break;
		case MWFilmRating_PG13:
			filmRatingText = MWFilmRating_PG13_Text;
			break;
		case MWFilmRating_R:
			filmRatingText = MWFilmRating_R_Text;
			break;
		case MWFilmRating_NC17:
			filmRatingText = MWFilmRating_NC17_Text;
			break;
		default:
			filmRatingText = MWFilmRating_Unknown_Text;
			break;
	}
	
	_filmRatingText = filmRatingText;
	
	// Rating
	NSNumber *rating = [NSNumber numberWithFloat:film.rating];
	
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	formatter.locale = [NSLocale currentLocale];
	formatter.numberStyle = NSNumberFormatterDecimalStyle;
	formatter.usesGroupingSeparator = YES;
	formatter.maximumFractionDigits = 1; // for instance 7.8  or 0.## format
	
	_ratingText = [formatter stringFromNumber:rating];
	
	return self;
}

- (NSString *)directorNameText {
	NSString *directorName;
	
	if (!self.film.director) {
		directorName = @"No director";
	} else if ([self.film.director.name stringIsNilOrEmpty]) {
		directorName = @"No director name" ;
	} else {
		directorName = self.film.director.name;
	}
	
	return directorName;
}

- (NSString *)actorNameText {
	Actor *actor = [self.film.casts firstObject];
	NSString *actorName =  @"No actor";
	
	if (actor) {
		ActorViewModel *actorViewModel = [[ActorViewModel alloc] initWithActor:actor];
		actorName = actorViewModel.actorNameText;
	}
	
	return actorName;
}

- (NSString *)actorScreenNameText {
	Actor *actor = [self.film.casts firstObject];
	NSString *actorScreenName = @"No actor";
	
	if (actor) {
		ActorViewModel *actorViewModel = [[ActorViewModel alloc] initWithActor:actor];
		actorScreenName = actorViewModel.actor.screenName;
	}
	
	return actorScreenName;
}

- (void)setDirectorNameText:(NSString *)directorNameText {
	if (![self.film.director.name isEqualToString:directorNameText]) {
		[self willChangeValueForKey:NSStringFromSelector(@selector(directorNameText))]; // KVO
		self.film.director.name = directorNameText;
		[self didChangeValueForKey:NSStringFromSelector(@selector(directorNameText))];  // KVO
	}
}

- (void)setActorNameText:(NSString *)actorNameText {
	Actor *actor = [self.film.casts firstObject];
	
	if (![actor.name isEqualToString:actorNameText]) {
		[self willChangeValueForKey:NSStringFromSelector(@selector(actorNameText))]; // KVO
		actor.name = actorNameText;
		[self didChangeValueForKey:NSStringFromSelector(@selector(actorNameText))];  // KVO
	}
}

- (void)setActorScreenNameText:(NSString *)actorScreenNameText {
	Actor *actor = [self.film.casts firstObject];
	
	if (![actor.screenName isEqualToString:actorScreenNameText]) {
		[self willChangeValueForKey:NSStringFromSelector(@selector(actorScreenNameText))];  // KVO
		actor.screenName = actorScreenNameText;
		[self didChangeValueForKey:NSStringFromSelector(@selector(actorScreenNameText))];  // KVO
	}
}

@end
