//
//  DirectorViewModel.m
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "DirectorViewModel.h"
#import "FilmViewModel.h"

@implementation DirectorViewModel

- (instancetype)initWithDirector:(Director *)director {
	self = [super init];
	if (!self) return nil;
	
	_director = director;
	
	// Name
	_nameText = [director.name stringIsNilOrEmpty] ? @"No name" : director.name;
	
	_film = director.film;
			
	return self;
}

@end
