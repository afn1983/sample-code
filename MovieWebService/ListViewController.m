//
//  ViewController.m
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "ListViewController.h"
#import "MovieTableViewCell.h"
#import "ListFilmViewModel.h"
#import "DetailViewController.h"

// Constants
static NSString *const MWMovieViewCellId = @"MovieCellId";		// table view cell id
static CGFloat const MWEstimatedRowHeight	= 60.0f;			// Estimated row height
static void* const MyKVOContext = (void *)&MyKVOContext;		// an arbitrary pointer that points to itself

@interface ListViewController() <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UINib *movieCellView;
@property (strong, nonatomic) ListFilmViewModel *listFilm;

@end

@implementation ListViewController

- (void)dealloc {
	[self.listFilm removeObserver:self forKeyPath:NSStringFromSelector(@selector(films)) context:MyKVOContext];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.listFilm = [ListFilmViewModel shared];
	
	// register nib file
	self.movieCellView = [UINib nibWithNibName:NSStringFromClass([MovieTableViewCell class]) bundle:nil];
	[self.tableView registerNib:self.movieCellView forCellReuseIdentifier:MWMovieViewCellId];
	
	// dynamic rows
	self.tableView.estimatedRowHeight = MWEstimatedRowHeight;
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	
	[self setupKVO];
}

#pragma mark - KVO

- (void)setupKVO {
	[self.listFilm addObserver:self forKeyPath:NSStringFromSelector(@selector(films)) options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:MyKVOContext];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
	if ([object isKindOfClass:[ListFilmViewModel class]]) {
		if ([keyPath isEqualToString:NSStringFromSelector(@selector(films))]) {
			// the list of films has changed, we need to reload the table view.
			self.listFilm = [change objectForKey:@"new"];
			[self.tableView reloadData];
		}
	}
}

#pragma mark - UITableViewDelegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listFilm.films.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MWMovieViewCellId];
	
	FilmViewModel *filmViewModel = [self.listFilm.films objectAtIndex:indexPath.row];
    [cell setupCell:filmViewModel];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	FilmViewModel *filmViewModel = [self.listFilm.films objectAtIndex:indexPath.row];
	DetailViewController *detailViewController = [[DetailViewController alloc] initWithFilmViewModel:filmViewModel];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

@end
