//
//  ActorViewModel.h
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "Actor.h"

@interface ActorViewModel : NSObject

@property (nonatomic, readonly) Actor *actor;
@property (nonatomic, readonly) NSString *actorNameText;
@property (nonatomic, readonly) NSString *screenNameText;

- (instancetype)initWithActor:(Actor *)actor;

@end
