//
//  TappableLabel.h
//  MovieWebService
//
//
//

@protocol TappableLabelDelegate;

@interface TappableLabel : UILabel

@property (nonatomic, weak) id<TappableLabelDelegate> delegate;

@end

@protocol TappableLabelDelegate <NSObject>

@optional

- (void)onDidReceiveTouch:(TappableLabel *)tappableLabel;

@end
