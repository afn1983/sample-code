//
//  Film.h
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "Actor.h"
#import "Director.h"

typedef NS_ENUM (NSInteger, MWFilmRatingType) {
    MWFilmRating_G,
    MWFilmRating_PG,
    MWFilmRating_PG13,
    MWFilmRating_R,
    MWFilmRating_NC17
};

@interface Film : NSObject

@property (nonatomic, assign)	MWFilmRatingType	filmRating;
@property (nonatomic, strong)	NSArray				*languages;
@property (nonatomic, strong)	NSDate				*releaseDate;
@property (nonatomic, strong)	NSArray<Actor*>		*casts;

@property (nonatomic, strong)	NSString			*name;
@property (nonatomic, assign)	float				rating;
@property (nonatomic, assign)	BOOL				nominated;
@property (nonatomic, strong)	Director			*director;

- (instancetype)initWithData:(NSDictionary *)data;

@end

