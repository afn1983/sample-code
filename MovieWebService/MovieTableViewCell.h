//
//  MovieTableViewCell.h
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "FilmViewModel.h"

@interface MovieTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *filmRating;
@property (strong, nonatomic) IBOutlet UILabel *rating;

- (void)setupCell:(FilmViewModel *)filmViewModel;

@end
