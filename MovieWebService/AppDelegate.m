//
//  AppDelegate.m
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "AppDelegate.h"
#import "ListFilmViewModel.h"

@implementation AppDelegate

// Shared instance of the app delegate class
+ (AppDelegate *)sharedAppDelegate {
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window.rootViewController = self.navigationController;
    self.navigationController.navigationBar.translucent = NO;
	
	// Async method that creates a list of films
	[[ListFilmViewModel shared] loadSampleData:nil];
		
    return YES;
}

@end
