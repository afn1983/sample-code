//
//  NSString+Extentions.h
//  MovieWebService
//
//  Created by Alex Núñez on 17/10/2016.
// 
//

@interface NSString (Extentions)

- (BOOL)stringIsNilOrEmpty;
	
@end
