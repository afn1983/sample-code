//
//  ListFilmViewModel.h
//  MovieWebService
//
//  Created by Alex Núñez on 15/10/2016.
// 
//

#import "Film.h"

@interface ListFilmViewModel : NSObject

@property (nonatomic, readonly) NSMutableArray<Film *> *films;

+ (id)shared;

- (void)loadSampleData:(void (^)(BOOL finished))completionHandler;

@end
