//
//  NSString+Extentions.m
//  MovieWebService
//
//  Created by Alex Núñez on 17/10/2016.
// 
//

#import "NSString+Extentions.h"

@implementation NSString (Extentions)

- (BOOL)stringIsNilOrEmpty {
	return !self.length;
}

@end
