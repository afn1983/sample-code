//
//  Actor.h
//  MovieWebService
//
// 
//  
//

#import "GenericRole.h"

@interface Actor : GenericRole

@property (nonatomic, strong) NSString *screenName;

- (instancetype)initWithData:(NSDictionary *)data;

@end
