//
//  AppDelegate.h
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "Film.h"

@interface AppDelegate : NSObject <UIApplicationDelegate>

@property (weak, nonatomic) IBOutlet UIWindow *window;
@property (weak, nonatomic) IBOutlet UINavigationController *navigationController;

+ (AppDelegate *)sharedAppDelegate;

@end
