//
//  DetailViewController.m
//  MovieWebService
//
// on 3/10/14.
//  
//

#import "DetailViewController.h"

// an arbitrary pointer that points to itself
static void* const MyKVOContext = (void *)&MyKVOContext;

@interface DetailViewController ()

@property (nonatomic, strong) FilmViewModel *filmViewModel;

@end

@implementation DetailViewController

- (void)dealloc {
	[self.filmViewModel removeObserver:self forKeyPath:NSStringFromSelector(@selector(directorNameText)) context:MyKVOContext];
	[self.filmViewModel removeObserver:self forKeyPath:NSStringFromSelector(@selector(actorNameText)) context:MyKVOContext];
	[self.filmViewModel removeObserver:self forKeyPath:NSStringFromSelector(@selector(actorScreenNameText)) context:MyKVOContext];
}

- (instancetype)initWithFilmViewModel:(FilmViewModel *)filmViewModel {
	self = [super init];
	if (self) {
		_filmViewModel = filmViewModel;
		[self setupKVO];
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.backBarButtonItem.title = @"Back"; // when landscape it will show the full topviewcontroller title
	
	self.tapToShowMore.delegate = self;
	
	[self configureView];
}

- (void)configureView {
	self.directorName.text		= self.filmViewModel.directorNameText;
	self.actorName.text			= self.filmViewModel.actorNameText;
	self.actorScreenName.text	= self.filmViewModel.actorScreenNameText;
}

#pragma mark - KVO

// in order to avoid Signals or 3rd party libraries for this simple project I'm using the KVO approach in order to binding the data.
- (void)setupKVO {
	[self.filmViewModel addObserver:self forKeyPath:NSStringFromSelector(@selector(directorNameText)) options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:MyKVOContext];
	[self.filmViewModel addObserver:self forKeyPath:NSStringFromSelector(@selector(actorNameText)) options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:MyKVOContext];
	[self.filmViewModel addObserver:self forKeyPath:NSStringFromSelector(@selector(actorScreenNameText)) options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:MyKVOContext];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
	if ([object isKindOfClass:[FilmViewModel class]]) {
		if ([keyPath isEqualToString:NSStringFromSelector(@selector(directorNameText))]) {
			self.directorName.text = [change objectForKey:@"new"];
		} else 	if ([keyPath isEqualToString:NSStringFromSelector(@selector(actorNameText))]) {
			self.actorName.text = [change objectForKey:@"new"];
		} else 	if ([keyPath isEqualToString:NSStringFromSelector(@selector(actorScreenNameText))]) {
			self.actorScreenName.text = [change objectForKey:@"new"];
		}
	}
}

#pragma mark - TappableLabelDelegate

- (void)onDidReceiveTouch:(TappableLabel *)tappableLabel {
    self.tapToShowMore.hidden			= YES;
    self.actorName.hidden				= NO;
    self.actorScreenName.hidden			= NO;
    self.actorNameTitle.hidden			= NO;
    self.actorScreenNameTitle.hidden	= NO;
}

@end
