//
//  ActorViewModelTests.m
//  MovieWebService
//
//  Created by Alex Núñez on 16/10/2016.
// 
//

#import <XCTest/XCTest.h>
#import "ActorViewModel.h"

@interface ActorViewModelTests : XCTestCase

@end

@implementation ActorViewModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Actor Parsing Tests

- (void)testCreateActorViewModelWithEmptyDictionary
{
	NSDictionary *testDict = @{};
	Actor *actor = [[Actor alloc] initWithData:testDict];
	XCTAssertNotNil(actor);
	
	ActorViewModel *actorViewModel = [[ActorViewModel alloc] initWithActor:actor];
	XCTAssertNotNil(actorViewModel);
}

- (void)testActorWithOnlyStringData
{
	NSDictionary *testDict = @{
							   @"dateOfBirth": @"string",
							   @"nominated": @"string",
							   @"name": @"",
							   @"screenName": @"name",
							   @"biography": @"biography"
							   };
	
	XCTAssertNotNil([[Actor alloc] initWithData:testDict]);
}

- (void)testNominated
{
	NSDictionary *testDict = @{
							   @"dateOfBirth": @"string",
							   @"nominated": @3,
							   @"name": @"m name",
							   @"screenName": @"screen name",
							   @"biography": @"biography"
							   };
	
	Actor *actor = [[Actor alloc] initWithData:testDict];
	XCTAssertTrue(actor.nominated, "The nominated property should be true when you try to pass nomiated >= 1");
	
	testDict = @{
							   @"dateOfBirth": @"string",
							   @"nominated": @0,
							   @"name": @"m name",
							   @"screenName": @"screen name",
							   @"biography": @"biography"
							   };
	
	actor = [[Actor alloc] initWithData:testDict];
	XCTAssertFalse(actor.nominated, "The nominated property should be false when passing 0");
}

- (void)testScreenName
{
	NSDictionary *testDict = @{
							   @"dateOfBirth": @"string",
							   @"nominated": @1,
							   @"name": @"Albert",
							   @"screenName": @"",
							   @"biography": @"biography"
							   };
	
	Actor *actor = [[Actor alloc] initWithData:testDict];
	ActorViewModel *actorViewModel = [[ActorViewModel alloc] initWithActor:actor];
	
	XCTAssertTrue([actorViewModel.screenNameText isEqualToString:@"Albert"], "The screen name is empty so the property should be the actor's name field instead [Albert]");
}
@end
