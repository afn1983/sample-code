//
//  DirectorViewModelTests.m
//  MovieWebService
//
//  Created by Alex Núñez on 16/10/2016.
// 
//

#import <XCTest/XCTest.h>
#import "DirectorViewModel.h"

@interface DirectorViewModelTests : XCTestCase

@end

@implementation DirectorViewModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Director Tests

- (void)testCreateDirectorViewModelWithEmptyDictionary
{
	NSDictionary *testDict = @{};
	Director *director = [[Director alloc] initWithData:testDict];
	XCTAssertNotNil(director);
	
	DirectorViewModel *directorViewModel = [[DirectorViewModel alloc] initWithDirector:director];
	XCTAssertNotNil(directorViewModel);
}

- (void)testDirectorWithOnlyStringData
{
	NSDictionary *testDict = @{
							   @"dateOfBirth": @"string test",
							   @"nominated": @"string",
							   @"name": @"name test",
							   @"biography": @"string test "
							   };
	
	XCTAssertNotNil([[Director alloc] initWithData:testDict]);
}

- (void)testDirectorEmptyName
{
	Director *director = [[Director alloc] init];
	director.name = @"";
	
	DirectorViewModel *directorViewModel = [[DirectorViewModel alloc] initWithDirector:director];
	
	XCTAssertTrue([directorViewModel.nameText isEqualToString:@"No name"], "If the director name is empty the nameText should be 'No name'");
}

@end
