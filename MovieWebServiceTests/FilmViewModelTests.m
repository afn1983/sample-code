//
//  FilmViewModelTests.m
//  MovieWebServiceTests
//
// on 3/10/14.
//  
//

#import <XCTest/XCTest.h>
#import "ListFilmViewModel.h"
#import "FilmViewModel.h"

@interface FilmViewModelTests : XCTestCase

@property (nonatomic, strong) Film *sampleFilm;

@end

@implementation FilmViewModelTests

- (void)setUp
{
	NSDictionary *filmADict = @{
								@"filmRating" : @3,
								@"languages": @[
										@"English",
										@"Thai"
										],
								@"nominated": @1,
								@"releaseDate": @1350000000,
								@"cast": @[
										@{
											@"dateOfBirth": @-436147200,
											@"nominated": @1,
											@"name": @"Bryan Cranston",
											@"screenName": @"Jack Donnell",
											@"biography": @"Bryan Lee Cranston is an American actor, voice actor, writer and director."
											}
										],
								@"name": @"Argo",
								@"rating": @7.8,
								@"director": @{
										@"dateOfBirth": @82684800,
										@"nominated": @1,
										@"name": @"Ben Affleck",
										@"biography": @"Benjamin Geza Affleck was born on August 15, 1972 in Berkeley, California, USA but raised in Cambridge, Massachusetts, USA."
										}
								};
	
	self.sampleFilm = [[Film alloc] initWithData:filmADict];
	
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}
									  
#pragma mark - Film Tests

- (void)testFilmInitialization
{
	Film *film = [[Film alloc] init];
	
	// Initialize FilmViewModel View Model
	FilmViewModel *filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertNotNil(filmViewModel); // the film shouldn't be nil
	
	XCTAssertTrue(filmViewModel.film == film, @"The profile should be equal to the item that was passed in.");
}

- (void)testFilmWithOnlyStringData
{
	NSDictionary *testFilmDict = @{
								@"filmRating" : @"string",
								@"languages": @[
										@"English",
										@"Thai"
										],
								@"nominated": @"string",
								@"releaseDate": @"string",
								@"cast": @[
										@{
											@"dateOfBirth": @"string",
											@"nominated": @"string",
											@"name": @"",
											@"screenName": @"name",
											@"biography": @"biography"
											}
										],
								@"name": @"",
								@"rating": @"string",
								@"director": @{
										@"dateOfBirth": @"string",
										@"nominated": @"string",
										@"name": @"Ben Affleck",
										@"biography": @""
										}
								};
	
	
	XCTAssertNotNil([[Film alloc] initWithData:testFilmDict]);
}

- (void)testFilmRatingFormat
{
	Film *film = [[Film alloc] init];
	film.rating = 6.9111222;
	
	FilmViewModel *filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	
	XCTAssertTrue([filmViewModel.ratingText isEqualToString:@"6.9"], "The formatted rating should be equal to 6.9");
}

- (void)testFilmRatingRound
{
	Film *film = [[Film alloc] init];
	film.rating = 6.8999;
	
	FilmViewModel *filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	
	XCTAssertTrue([filmViewModel.ratingText isEqualToString:@"6.9"], "The formatted rating should be equal to 6.9");
}

- (void)testFilmFilmRating
{
	Film *film = [[Film alloc] init];
	film.filmRating = MWFilmRating_PG;
	
	FilmViewModel *filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.filmRatingText isEqualToString:MWFilmRating_PG_Text], "The formatted film rating should be equal to PG");
	
	film.filmRating = MWFilmRating_G;
	
	filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.filmRatingText isEqualToString:MWFilmRating_G_Text], "The formatted film rating should be equal to G");
	
	film.filmRating = MWFilmRating_NC17;
	
	filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.filmRatingText isEqualToString:MWFilmRating_NC17_Text], "The formatted film rating should be equal to NC17");
	
	film.filmRating = MWFilmRating_R;
	
	filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.filmRatingText isEqualToString:MWFilmRating_R_Text], "The formatted film rating should be equal to R");
	
	film.filmRating = MWFilmRating_PG13;
	
	filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.filmRatingText isEqualToString:MWFilmRating_PG13_Text], "The formatted film rating should be equal to PG13");
	
	film.filmRating = 10;
	
	filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.filmRatingText isEqualToString:MWFilmRating_Unknown_Text], "The formatted film rating should be equal to Unknown");
}

- (void)testFilmEmptyNames
{
	Film *film = [[Film alloc] init];
	film.name = @"";
	
	FilmViewModel *filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.nameText isEqualToString:@"No name"], "If the film's name is empty the nameText should be 'No name'");
	
	XCTAssertTrue([filmViewModel.directorNameText isEqualToString:@"No director"], "If the film's director is s empty the directorNameText should be 'No director'");
	XCTAssertTrue([filmViewModel.actorNameText isEqualToString:@"No actor"], "If the film's actor is s empty the actorNameText should be 'No actor'");
	XCTAssertTrue([filmViewModel.actorScreenNameText isEqualToString:@"No actor"], "If the film's actor is s empty the actorScreenNameText should be 'No actor'");
}

- (void)testFilmNilName
{
	Film *film = [[Film alloc] init];
	film.name = nil;
	
	FilmViewModel *filmViewModel = [[FilmViewModel alloc] initWithFilm:film];
	XCTAssertTrue([filmViewModel.nameText isEqualToString:@"No name"], "If the film's name is nil the nameText should be 'No name'");
}

- (void)testFilmWithSpecialCharacters
{
	// Japanese
	NSDictionary *filmADict = @{
								@"filmRating" : @3,
								@"languages": @[
										@"イギリス",
										@"Thai"
										],
								@"nominated": @1,
								@"releaseDate": @1350000000,
								@"cast": @[
										@{
											@"dateOfBirth": @-436147200,
											@"nominated": @1,
											@"name": @"たなかさん",
											@"screenName": @"たなか",
											@"biography": @"たなかさんは　いえに　はえた."
											}
										],
								@"name": @"きみのなまえ",
								@"rating": @7.8,
								@"director": @{
										@"dateOfBirth": @82684800,
										@"nominated": @1,
										@"name": @"ただし",
										@"biography": @"ただしさん"
										}
								};
	
	XCTAssertNotNil([[Film alloc] initWithData:filmADict]);
}

@end
